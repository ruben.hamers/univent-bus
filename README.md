# Univent-Bus

Light weight Unity event bus

## Introduction
This is a light weight event bus for Unity3D. It is a topic based publish and subscribe bus where events are fully customizable. 
Events can be both persistent and non-persistent; Persisted events can be created by using the `PersistentEvent` class or making your own persistent event by implementing the `IPersistableEvent` interface.
Meaning; events of type `IEvent` will not get persisted on the Bus, but will simply be fired and discarded, so if you want to be able to access the event later in time, you must implement the `IpersistableEvent` interface.

## Usage
This library is designed to be used through custom extension methods. <I>Never</i> Should you be needed to directly call `EventBus.<whatever-method>`, with the exception of using the bus during editor-time (more on that later). All logic can be accessed by extension methods:
### Subscribing:
To subscribe you can use the `this.subscribe` extension which has multiple overloads depending on your needs.
Also, subscribing to a topic can be done in a static and non-static sense meaning; `static` subscriptions will not get purged when the `ActiveScene` unloads, normal subscribes will be removed when the `ActiveScene` unloads.
Effectively this means, that everything that is bound to the life-time of the scene, like Monobehaviours should simply subscribe.
If you have any `Manager`/ `Controller` type classes that are `DontDestroyOnLoad`/`static classes` you can statically subscribe and their subscriptions will not get cleared. 
#### Simple subscription
```c#
string subscriberId = this.Subscribe(
    "foo", 
    evt => { Debug.Log($"Event {evt} with data {evt.Get();} on topic foo was just caught!"); });
```
The above example shows how to subscribe to a topic called "foo" and simply logs all events that are caught to the unity console. You can also use `bool event.TryGet<T>(out value)` if you are not sure what the datatype is for the actual data.
The subscribe function also returns an ID you can later use to unsubscribe from the bus.
#### Filtered subscription
```c#
// type filter on <MyAwesomeEvent>
string subscriberId = this.Subscribe<MyAwesomeEvent>(
    "foo", 
    evt => { Debug.Log("Event caught!"); });

// type filter on MyAwesomeEvent + additional lambda to check if the AwesomeEvent IsSuperAwesome.
string subscriberId = this.Subscribe<MyAwesomeEvent>(
    "foo", 
    evt => { Debug.Log("Event caught!"); }, 
    evt => { return evt.IsSuperAwesome();});
```
The above example shows how to subscribe to topic "foo" and listen for an event of type `MyAwesomeEvent' + an additional filter to only get the AwesomeEvents that are SuperAwesome.
So a filter can used by type, plus an additional <b>boolean lambda function/filter</b>
#### Filtered subscription + Seek in history
```c#
string subscriberId = this.Subscribe<MyAwesomeEvent>(
    "foo", 
    evt => { Debug.Log("Event caught!"); }, 
    evt => { return evt.IsSuperAwesome();},
    events => { events.ForEach(f=> Debug.Log($"Yes.. I Am Super Awesome!");)});
```
The above code shows how to not only subscribe to topic "foo" with a special filter, but also how to add a (lambda) function as a callback where the filter is applied to all events in the bus, of topic "foo" that were persisted.
#### Static subscription
```c#
string subscriberId = this.SubscribeStatic(
    "foo", 
    evt => { Debug.Log($"Event {evt} with data {evt.Get();} on topic foo was just caught!"); });
```
<i>*SubscribeStatic supports all of the overloads that the `Subscribe` method does.</i>
### Unsubscribing
So each of the overloads of the `this.Subscribe..` function return a specific ID.
This ID can be used to unsubscribe a method from the bus. I chose to implement it this way, since I did not want to add a reference to the object of the subscriber to the bus.
I think this is more light weight and a cleaner solution. (By adding like an `object sender` as a reference to the event bus we keep it in memory, also it the object is monobehaviour it will be destroyed once the scene reloads resulting in null-reference exceptions)
The simple lambda or functions can be added to the bus without worrying about the sender being null.
That being said:<b>You need to keep track of your own subscribers! So before each scene reload, you should think carefully what subscriptions you want to keep.</b>
```c#
if(!this.Unsubscribe(subscriberId))
    Debug.Log("Unsubscribe failed!, Do something special!");
```

### Publishing events
To publish an event on the bus you can use the `this.publish` extensions.
#### Simple publish
A simple publish, of a string of type`DefaultEvent` (non-persistable) on topic "foo" looks like:
```c#
"myData".Publish("foo");
```
#### Persistent publish
A persistent publish, of a string of type `PersistableEvent` (so this event will stay on the bus, and you can seek for it) on topic "foo" looks like:
```c#
"myData".Publish<PersistableEvent>("foo");
```

#### Custom event publish
A custom event publish, of a string of type `MyCustomEvent` on topic "foo" looks like:
```c#
"myData".Publish<MyCustomEvent>("foo");
```

#### Constructor params
Since some events will probably require more than one constructor arguments you can add additional `params` to the publish function.
Imagine you have an event like:
```c#
 private class EventWithConstructorArguments : PersistableMockEvent
        {
            public readonly bool Flag;
            public readonly string Name;
            public readonly int Number;
            public readonly List<DateTime> Dates;

            public EventWithConstructorArguments(object value, bool flag, string name, int number,
                List<DateTime> dates) : base(value)
            {
                Flag = flag;
                Name = name;
                Number = number;
                Dates = dates;
            }
        }

5.Publish<EventWithConstructorArguments>("foo"); // Will throw a MissingMethodException, since the class has no constructor with 1 argument.
5.Publish<EventWithConstructorArguments>("foo", "Uncle Bob", true, 1337, new List<DateTime>{DateTime.Now}); // Will throw a MissingMethodException, since the (flag and name) contructor arguments are not in order
5.Publish<EventWithConstructorArguments>("foo", true, "Uncle Bob", 1337, new List<DateTime>{DateTime.Now}); // Will succeed
```
So you can add more arguments <i>(in correct order!)</i> and they will be used as constructor arguments. If they do not match a constructor signature defined by your custom class a `MissingMethodException` will be thrown.
This also means that, if your custom event has only 1 constructor argument, and you supply it with the wrong one you will get this exception.
### Seeking
One of the overloads of the subscribe functions allows for seeking through the events of a certain topic on the event bus.
If you do not want to subscribe to a topic, but simply want to seek through a topic, you can use the `<string>.Seek()` function.
```c#
// If you again want to Seek on the bus, for events of type MyAwsomeEvent that are SuperAwesome on topic foo;
var events = "foo".Seek((evt) => { return evt.IsSuperAwesome();});
foreach (MyAwesomeEvent awesomeEvent in events)
    Debug.Log("Yeah.. I'm super awesome!");
```
## Topicless events
Topicless events are also supported. All previously described functions support overloads without specifying a topic.
Note that:
* When publishing a <b>topicless</b> event, only the topicless subscribers are notified since they are subscribers that specify a topic to use as a filter.
* When publishing an event <b>with a topic</b>, both the topicless subscribers and the subscribers that specify that specific topic are notified.
* When seeking for a topicless event, you get the results of both the events that where published with and without topic that match the filter.

## Run-time vs. Editor-time
The event bus is accessible both at run-time and editor-time. This means, you can use the bus for creating editor extensions.
One important notice though: <b>All events AND subscribers will be purged when you press play in the editor!</b>

### Run-time
At run-tine, you do not need to do anything; The bus wil initialize itself <i>before scene start</i>. This is done through a function attribute (annotation) supplied by Unity (`[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]`).
Just remember that:
1. You, as a developer, need to unsubscribe from the bus when the life-time of your object has come to an end, IFF you subscribed Statically.
2. When the `ActiveScene` unloads, all non-static subscriptions are purged.
3. `OnApplicationQuit` will purge the bus of events and subscribers (stopping play-mode in editor will also trigger this event).

### Editor-Time
If you want to use the bus for editor scripting you only need to call the `EventBus.Init(force=true)` (editor only) function when you manually want to purge all events and subscribers.
This function will also flag the bus that it has been initialized, meaning; you can call the `Init(force=false)` function multiple times, with no side effects.
All other functions work exactly the same as during run-time.
<i>If you do not need to purge the events by force, you will never need to call the `EventBus.Init()` function.</i>

## Tests / Examples
For more detailed examples on usage please read through the event bus tests [here](Assets/Scripts/UniventBus/Tests/Editor/EventBusTests.cs).

## To-Do
* It would be nice to filter all subscriptions based on scene, meaning that only when that exact scene is unloaded the subscribers are purged for that scene only.
* Thread safety 