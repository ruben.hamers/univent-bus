using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using HamerSoft.UniventBus;
using NUnit.Framework;
using UnityEngine;

namespace UniventBus.Tests.Editor
{
    public class EventBusTests
    {
        private class MockEvent : DefaultEvent
        {
            public MockEvent(object value) : base(value)
            {
            }
        }

        private class PersistableMockEvent : PersistableEvent
        {
            public PersistableMockEvent(object value) : base(value)
            {
            }

            protected override void PreRelease()
            {
                base.PreRelease();
                Debug.Log("Do something special before the event is released from the stream!");
            }

            protected override void PostRelease()
            {
                base.PostRelease();
                Debug.Log("Do something special after the event is released from the stream!");
            }
        }

        private class MockEventWithConstructorArguments : PersistableMockEvent
        {
            public readonly bool Flag;
            public readonly string Name;
            public readonly int Number;
            public readonly List<DateTime> Dates;

            public MockEventWithConstructorArguments(object value, bool flag, string name, int number,
                List<DateTime> dates) : base(value)
            {
                Flag = flag;
                Name = name;
                Number = number;
                Dates = dates;
            }
        }

        private class IntegerMockEvent : IEvent
        {
            private readonly int _value;

            public IntegerMockEvent(int value)
            {
                _value = value;
            }

            public dynamic Get()
            {
                return _value;
            }

            public bool TryGet<T>(out T data)
            {
                data = default;
                if (!(_value is T value))
                    return false;
                data = value;
                return true;
            }
        }

        [SetUp]
        public void Setup()
        {
            EventBus.Init(true);
        }

        [TearDown]
        public void Teardown()
        {
        }

        /// <summary>
        /// calls through reflection to avoid having to make the function public
        /// (could be internal though.., but this shouldn't be accessible even internally.)
        /// </summary>
        private static void InvokeClearEvents()
        {
            typeof(EventBus).GetMethod("ClearDefaultEvents", BindingFlags.Static | BindingFlags.NonPublic)
                ?.Invoke(null, null);
        }

        [Test]
        public void After_Subscribing_To_Event_The_Subscriber_Is_Notified_When_The_Event_Is_Fired()
        {
            this.Subscribe("foo", evt => { Assert.Pass("Event is fired"); });
            "bar".Publish("foo");
        }

        [Test]
        public void After_SubscribeStatic_To_Event_The_Subscriber_Is_Notified_When_The_Event_Is_Fired()
        {
            this.SubscribeStatic("foo", evt => { Assert.Pass("Event is fired"); });
            "bar".Publish("foo");
        }

        [Test]
        public void When_StringEvent_Is_Caught_The_Value_Is_Correct()
        {
            this.Subscribe("foo", evt => { Assert.AreEqual("bar", evt.Get()); });
            "bar".Publish("foo");
        }

        [Test]
        public void When_SubscribeStatic_StringEvent_Is_Caught_The_Value_Is_Correct()
        {
            this.SubscribeStatic("foo", evt => { Assert.AreEqual("bar", evt.Get()); });
            "bar".Publish("foo");
        }

        [Test]
        public void When_StringEvent_Is_Caught_The_Value_From_TryGet_Is_Correct()
        {
            this.Subscribe("foo", evt =>
            {
                if (evt.TryGet<string>(out var result))
                    Assert.AreEqual("bar", result);
                else
                    Assert.Fail("Could not convert values");
            });
            "bar".Publish("foo");
        }

        [Test]
        public void When_SubScribeStatic_StringEvent_Is_Caught_The_Value_From_TryGet_Is_Correct()
        {
            this.SubscribeStatic("foo", evt =>
            {
                if (evt.TryGet<string>(out var result))
                    Assert.AreEqual("bar", result);
                else
                    Assert.Fail("Could not convert values");
            });
            "bar".Publish("foo");
        }

        [Test]
        public void When_StringEvent_Is_Caught_The_Value_From_TryGet_Is_InCorrect_When_The_Invalid_Type_Is_Given()
        {
            this.Subscribe("foo", evt =>
            {
                if (evt.TryGet<bool>(out var result))
                    Assert.Fail("hmm, string != bool, yet the test still passes");
                else
                    Assert.Pass("Yes indeed, cannot cast a string to a bool");
            });
            "bar".Publish("foo");
        }

        [Test]
        public void
            When_SubscribeStatic_StringEvent_Is_Caught_The_Value_From_TryGet_Is_InCorrect_When_The_Invalid_Type_Is_Given()
        {
            this.SubscribeStatic("foo", evt =>
            {
                if (evt.TryGet<bool>(out var result))
                    Assert.Fail("hmm, string != bool, yet the test still passes");
                else
                    Assert.Pass("Yes indeed, cannot cast a string to a bool");
            });
            "bar".Publish("foo");
        }

        [Test]
        public void After_Unsubscribe_The_Event_Is_No_Longer_Caught()
        {
            int count = 0;
            string subscriberId = this.Subscribe("foo", evt =>
            {
                Assert.Pass("Event is fired");
                count++;
            });
            "bar".Publish("foo");
            Assert.True(this.Unsubscribe(subscriberId));
            "bar".Publish("foo");
            Assert.AreEqual(1, count);
        }

        [Test]
        public void After_Static_Is_Unsubscribed_The_Event_Is_No_Longer_Caught()
        {
            int count = 0;
            string subscriberId = this.SubscribeStatic("foo", evt =>
            {
                Assert.Pass("Event is fired");
                count++;
            });
            "bar".Publish("foo");
            Assert.True(this.Unsubscribe(subscriberId));
            "bar".Publish("foo");
            Assert.AreEqual(1, count);
        }

        [Test]
        public void When_Subscribed_With_Filter_Events_That_Do_Not_Match_The_Predicate_Are_Not_Caught()
        {
            int count = 0;
            string subscriberId = this.Subscribe<PersistableEvent>("foo",
                evt => { count++; },
                Filter);
            "bar".Publish("foo");
            new PersistableEvent("baz").Publish("foo");
            Assert.AreEqual(1, count);
        }

        [Test]
        public void When_Event_Is_Persistable_But_There_Is_Nothing_Published_The_Result_Is_An_Empty_List()
        {
            this.Subscribe<PersistableEvent>("foo",
                evt => { },
                evt => false,
                mockEvents => { Assert.AreEqual(0, mockEvents.Count); });
        }

        [Test]
        public void When_Event_Is_Persistable_You_Can_Query_For_It()
        {
            new PersistableEvent("bar").Publish("foo");
            new PersistableEvent("baz").Publish("foo");
            new PersistableEvent("qux").Publish("foo");
            new MockEvent("quux").Publish("foo");

            this.Subscribe<PersistableEvent>("foo",
                evt => { },
                Filter,
                mockEvents => { Assert.AreEqual(3, mockEvents.Count); });
        }

        [Test]
        public void When_Event_Is_Persistable_But_There_Is_Nothing_Published_The_Result_Of_Seek_Is_An_Empty_List()
        {
            Assert.AreEqual(0, "foo".Seek<PersistableEvent>(Filter).Count);
        }

        [Test]
        public void When_Event_Is_Persistable_You_Can_Seek_For_It()
        {
            new PersistableEvent("bar").Publish("foo");
            new PersistableEvent("baz").Publish("foo");
            new PersistableEvent("qux").Publish("foo");
            new MockEvent("quux").Publish("foo");

            Assert.AreEqual(3, "foo".Seek<PersistableEvent>(Filter).Count);
        }

        [Test]
        public void When_Event_Is_Released_You_Can_No_Longer_Seek_For_It()
        {
            new PersistableEvent("bar").Publish("foo");
            new PersistableEvent("baz").Publish("foo");
            new PersistableEvent("qux").Publish("foo");
            var events = "foo".Seek<PersistableEvent>(Filter);
            Assert.AreEqual(3, events.Count);
            events.First().Release();
            Assert.AreEqual(2, "foo".Seek<PersistableEvent>(Filter).Count);
        }

        [Test]
        public void When_Publishing_Events_Through_Extensions_The_Constructor_Argument_Is_Set_Correctly()
        {
            Assert.DoesNotThrow(() => 1.Publish<IntegerMockEvent>("foo"));
        }

        [Test]
        public void
            When_Publishing_Events_Through_Extensions_With_An_Invalid_Constructor_Argument_An_Exception_Is_Thrown()
        {
            Assert.Throws<MissingMethodException>(() =>
                "I am a string not an integer".Publish<IntegerMockEvent>("foo"));
        }

        [Test]
        public void When_Publishing_Events_Through_Extensions_The_Constructor_Arguments_Are_Set_Correctly()
        {
            bool flag = true;
            string name = "Uncle Bob";
            int number = 1337;
            List<DateTime> dates = new List<DateTime>() {DateTime.Now};
            this.Subscribe<MockEventWithConstructorArguments>("foo", evt =>
            {
                Assert.AreEqual(5, evt.Get());
                Assert.AreEqual(flag, evt.Flag);
                Assert.AreEqual(name, evt.Name);
                Assert.AreEqual(number, evt.Number);
                Assert.AreEqual(dates, evt.Dates);
            });
            5.Publish<MockEventWithConstructorArguments>("foo", flag, name, number, dates);
        }

        [Test]
        public void
            When_Publishing_Events_Through_Extensions_With_Invalid_Constructor_Arguments_Will_Throw_An_Exception()
        {
            bool flag = true;
            string name = "Uncle Bob";
            int number = 1337;
            List<DateTime> dates = new List<DateTime>() {DateTime.Now};
            // constructor arguments name and flag are swapped!
            Assert.Throws<MissingMethodException>(() =>
                5.Publish<MockEventWithConstructorArguments>("foo", name, flag, number, dates));
        }

        [Test]
        public void When_Init_With_Force_Is_Called_Events_And_Subscribers_Are_Purged()
        {
            int count = 0;
            var subscriberId = this.Subscribe("foo", evt => { count++; });
            5.Publish<PersistableEvent>("foo");
            EventBus.Init(true);
            Assert.AreEqual(0, "foo".Seek<PersistableEvent>(evt => true).Count);
            5.Publish<PersistableEvent>("foo");
            Assert.AreEqual(1, count);
        }

        [Test]
        public void When_Init_Without_Force_Is_Called_Events_And_Subscribers_Are_Purged()
        {
            int count = 0;
            var subscriberId = this.Subscribe("foo", evt => { count++; });
            5.Publish<PersistableEvent>("foo");
            EventBus.Init();
            EventBus.Init();
            Assert.AreEqual(1, "foo".Seek<PersistableEvent>(evt => true).Count);
            5.Publish<PersistableEvent>("foo");
            Assert.AreEqual(2, count);
        }

        [Test]
        public void When_SubscribedStatic_The_StaticSubscribers_Will_Not_Get_Cleared_When_Scene_Is_Unloaded()
        {
            int count = 0;
            var subscriberId = this.SubscribeStatic("foo", evt =>
            {
                Assert.Pass("Great, caught event!");
                count++;
            });
            true.Publish("foo");
            InvokeClearEvents();
            "again".Publish("foo");
            Assert.AreEqual(2, count);
        }

        [Test]
        public void When_Subscribed_The_Subscribers_Will_Get_Cleared_When_Scene_Is_Unloaded()
        {
            int count = 0;
            var subscriberId = this.Subscribe("foo", evt =>
            {
                Assert.Pass("Great, caught event!");
                count++;
            });
            true.Publish("foo");
            InvokeClearEvents();
            "again".Publish("foo");
            Assert.AreEqual(1, count);
        }

        [Test]
        public void
            When_Firing_TopicLess_Event_Only_Subscribers_That_Listen_For_The_EventType_Without_Topic_Are_Notified()
        {
            int count = 0;
            this.Subscribe<MockEvent>(@event => { count++; });
            this.Subscribe<MockEvent>("foo", @event => { count++; });
            true.Publish<MockEvent>();
            Assert.AreEqual(1, count);
        }
        
        [Test]
        public void
            When_Firing_Event_With_Topic_Both_Subscribers_That_Listen_For_The_EventType_With_Or_Without_Topic_Are_Notified()
        {
            int count = 0;
            this.Subscribe<MockEvent>(@event => { count++; });
            this.Subscribe<MockEvent>("foo", @event => { count++; });
            true.Publish<MockEvent>("foo");
            Assert.AreEqual(2, count);
        }

        [Test]
        public void When_Seeking_For_TopicLess_Event_All_Events_Are_Found()
        {
            new PersistableEvent("my data").Publish();
            new PersistableEvent("my data").Publish("foo");
            Assert.AreEqual(2,this.Seek<PersistableEvent>(@event => true).Count);
        }

        private bool Filter(PersistableEvent evt)
        {
            Assert.Pass("Yeeeees yeees yees!");
            return true;
        }
    }
}