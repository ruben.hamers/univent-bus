using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace HamerSoft.UniventBus
{
    public static class Extensions
    {
        public static List<T> Seek<T>(this string topic, Func<T, bool> filter) where T : IEvent
        {
            return EventBus.Seek(topic, filter);
        }
        
        public static List<T> Seek<T>(this object caller, Func<T, bool> filter) where T : IEvent
        {
            return EventBus.Seek(filter);
        }

        public static void Publish(this object value, string topic, bool persistent = false)
        {
            EventBus.Publish(topic, persistent ? new PersistableEvent(value) : new DefaultEvent(value));
        }
        
        public static void Publish(this object value, bool persistent = false)
        {
            EventBus.Publish(persistent ? new PersistableEvent(value) : new DefaultEvent(value));
        }

        public static void Publish<T>(this object value, string topic, params object[] constructorArguments)
            where T : class, IEvent
        {
            if (constructorArguments == null || constructorArguments.Length == 0)
            {
                Publish<T>(value, topic);
                return;
            }

            object[] arguments = new List<object> {value}.Concat(constructorArguments).ToArray();
            try
            {
                T newEvent = Activator.CreateInstance(typeof(T), arguments) as T;
                EventBus.Publish(topic, newEvent);
            }
            catch (Exception e)
            {
                Debug.Log(
                    $"Could not create event of type {typeof(T)}, with constructor arguments {string.Join(", ", arguments)}, on topic {topic}.");
                throw;
            }
        }
        
        public static void Publish<T>(this object value, params object[] constructorArguments)
            where T : class, IEvent
        {
            if (constructorArguments == null || constructorArguments.Length == 0)
            {
                Publish<T>(value);
                return;
            }

            object[] arguments = new List<object> {value}.Concat(constructorArguments).ToArray();
            try
            {
                T newEvent = Activator.CreateInstance(typeof(T), arguments) as T;
                EventBus.Publish(newEvent);
            }
            catch (Exception e)
            {
                Debug.Log(
                    $"Could not create event of type {typeof(T)}, with constructor arguments {string.Join(", ", arguments)}, ");
                throw;
            }
        }

        public static void Publish<T>(this object value, string topic) where T : class, IEvent
        {
            try
            {
                T newEvent = Activator.CreateInstance(typeof(T), new[] {value}) as T;
                EventBus.Publish(topic, newEvent);
            }
            catch (Exception e)
            {
                Debug.Log($"Could not create event of type {typeof(T)}, with constructor arguments {value} on topic {topic}");
                throw;
            }
        }
        
        public static void Publish<T>(this object value) where T : class, IEvent
        {
            try
            {
                T newEvent = Activator.CreateInstance(typeof(T), new[] {value}) as T;
                EventBus.Publish(newEvent);
            }
            catch (Exception e)
            {
                Debug.Log($"Could not create event of type {typeof(T)}, with constructor arguments {value}");
                throw;
            }
        }

        public static void Publish(this IEvent evt, string topic)
        {
            EventBus.Publish(topic, evt);
        }
        
        public static void Publish(this IEvent evt)
        {
            EventBus.Publish(evt);
        }
        
        public static string Subscribe(this object subscriber, string topic, Action<IEvent> callback)
        {
            return EventBus.Subscribe(topic, callback);
        }
        
        public static string Subscribe(this object subscriber, Action<IEvent> callback)
        {
            return EventBus.Subscribe(callback);
        }
        
        public static string Subscribe<T>(this object subscriber, string topic, Action<T> callback) where T : IEvent 
        {
            return EventBus.Subscribe<T>(topic, callback);
        }
        
        public static string Subscribe<T>(this object subscriber, Action<T> callback) where T : IEvent 
        {
            return EventBus.Subscribe<T>(callback);
        }
        
        public static string Subscribe<T>(this object subscriber, string topic, Action<T> callback, Func<T, bool> filter = null,
            Action<List<T>> queryHandler = null) where T : IEvent
        {
            return EventBus.Subscribe<T>(topic, callback,filter,queryHandler);
        }
        
        public static string Subscribe<T>(this object subscriber, Action<T> callback, Func<T, bool> filter = null,
            Action<List<T>> queryHandler = null) where T : IEvent
        {
            return EventBus.Subscribe<T>(callback,filter,queryHandler);
        }
        
        public static string SubscribeStatic(this object subscriber, string topic, Action<IEvent> callback)
        {
            return EventBus.SubscribeStatic(topic, callback);
        }
        
        public static string SubscribeStatic(this object subscriber, Action<IEvent> callback)
        {
            return EventBus.SubscribeStatic(callback);
        }
        
        public static string SubscribeStatic<T>(this object subscriber, string topic, Action<T> callback) where T : IEvent 
        {
            return EventBus.SubscribeStatic<T>(topic, callback);
        }
        
        public static string SubscribeStatic<T>(this object subscriber, Action<T> callback) where T : IEvent 
        {
            return EventBus.SubscribeStatic<T>(callback);
        }
        
        public static string SubscribeStatic<T>(this object subscriber, string topic, Action<T> callback, Func<T, bool> filter = null,
            Action<List<T>> queryHandler = null) where T : IEvent
        {
            return EventBus.SubscribeStatic<T>(topic, callback,filter,queryHandler);
        }
        
        public static string SubscribeStatic<T>(this object subscriber, Action<T> callback, Func<T, bool> filter = null,
            Action<List<T>> queryHandler = null) where T : IEvent
        {
            return EventBus.SubscribeStatic<T>(callback,filter,queryHandler);
        }
        
        public static bool Unsubscribe(this object subscriber, string id)
        {
            return EventBus.Unsubscribe(id);
        }
        
        public static bool Unsubscribe(this string subscriberId)
        {
            return EventBus.Unsubscribe(subscriberId);
        }
    }
}