using System;

namespace HamerSoft.UniventBus
{
    public class PersistableEvent : DefaultEvent, IPersistableEvent
    {
        public event Action<IPersistableEvent> Released;

        public PersistableEvent(object value) : base(value)
        {
        }

        public void Release()
        {
            PreRelease();
            OnReleased();
            PostRelease();
        }

        private void OnReleased()
        {
            Released?.Invoke(this);
        }

        protected virtual void PreRelease()
        {
        }

        protected virtual void PostRelease()
        {
        }
    }
}