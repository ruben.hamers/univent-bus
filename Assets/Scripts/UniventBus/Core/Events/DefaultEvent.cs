namespace HamerSoft.UniventBus
{
    public class DefaultEvent : IEvent
    {
        private dynamic _value;

        public DefaultEvent(dynamic value)
        {
            _value = value;
        }

        public dynamic Get()
        {
            return _value;
        }

        public bool TryGet<T>(out T data)
        {
            data = default;
            if (!(_value is T value))
                return false;
            data = value;
            return true;
        }
    }
}