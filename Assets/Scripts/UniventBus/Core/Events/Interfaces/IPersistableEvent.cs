using System;

namespace HamerSoft.UniventBus
{
    public interface IPersistableEvent : IEvent
    {
        void Release();
        event Action<IPersistableEvent> Released;
    }
}