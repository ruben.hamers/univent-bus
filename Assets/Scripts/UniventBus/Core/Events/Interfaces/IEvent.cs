namespace HamerSoft.UniventBus
{
    public interface IEvent
    {
        dynamic Get();
        bool TryGet<T>(out T data);
    }
}