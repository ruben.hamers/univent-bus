﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HamerSoft.UniventBus
{
#if UNITY_EDITOR
    [InitializeOnLoad]
#endif
    public static class EventBus
    {
        /// <summary>
        /// a separator char used for generating unique ID's for subscriber in format:
        /// topic|guid
        /// We split the string to unsubscribe the guid from the correct topic 
        /// </summary>
        private static readonly char IdSeparator = '|';

        /// <summary>
        /// id for TopicLess 
        /// </summary>
        private static readonly string TopicLess = "t0P1c73$$";

        /// <summary>
        /// dictionary of events
        /// </summary>
        private static Dictionary<string, EventStream> _events;

#if UNITY_EDITOR
        /// <summary>
        /// keeps track if the bus is initialized
        /// </summary>
        private static bool _isInitialized;

        /// <summary>
        /// Init the event bus @Editor-time
        /// Only access this through scripts that live at EditorTime!
        /// </summary>
        /// <param name="force">force the bus to init</param>
        public static void Init(bool force = false)
        {
            if (_isInitialized && !force)
                return;
            _isInitialized = true;
            Clear();
            _events = new Dictionary<string, EventStream>();
        }

        /// <summary>
        /// subscribe to editor event to make sure we clear the event streams when entering playmode
        /// </summary>
        static EventBus()
        {
            EditorApplication.playModeStateChanged -= ClearIfEnteredPlayMode;
            EditorApplication.playModeStateChanged += ClearIfEnteredPlayMode;
        }

        /// <summary>
        /// Clean all previous events and subscribers if we entered play mode
        /// </summary>
        /// <param name="state"></param>
        private static void ClearIfEnteredPlayMode(PlayModeStateChange state)
        {
            Debug.Log($"Detected editor mode change! {state}");
            if (state != PlayModeStateChange.EnteredPlayMode)
                return;
            _isInitialized = false;
            Clear();
        }
#endif

        /// <summary>
        /// method that is invoked each time a scene is loaded, if the events are not yet initialized create a new dictonary
        /// </summary>
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void UnitySceneStarted()
        {
            if (_events != null)
                return;
            _events = new Dictionary<string, EventStream>();
            SceneManager.sceneUnloaded += SceneManagerOnSceneUnloaded_Handler;
            QuitHandler.Spawn(Clear);
        }

        private static void SceneManagerOnSceneUnloaded_Handler(Scene scene)
        {
            if (scene.name == SceneManager.GetActiveScene().name)
                ClearDefaultEvents();
        }

        private static void ClearDefaultEvents()
        {
            foreach (KeyValuePair<string, EventStream> pair in _events)
                pair.Value.ClearDefaultSubscribers();
        }

        /// <summary>
        /// clear all events and subscribers
        /// </summary>
        internal static void Clear()
        {
            SceneManager.sceneUnloaded -= SceneManagerOnSceneUnloaded_Handler;
            if (_events == null)
                return;
            foreach (KeyValuePair<string, EventStream> pair in _events)
                pair.Value.Clear();
            _events = new Dictionary<string, EventStream>();
        }

        /// <summary>
        /// public an event on the event bus
        /// </summary>
        /// <param name="topic">the topic it relates to</param>
        /// <param name="ev">the event object</param>
        internal static void Publish(string topic, IEvent ev)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                Init();
#endif
            CheckOrAddTopic(topic);
            CheckOrAddTopic(TopicLess);
            _events[topic].Publish(ev);
            _events[TopicLess].PublishTopicLess(ev);
        }

        /// <summary>
        /// public an event on the event bus without a topic
        /// </summary>
        /// <param name="ev">the event object</param>
        internal static void Publish(IEvent ev)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                Init();
#endif
            CheckOrAddTopic(TopicLess);
            _events[TopicLess].Publish(ev);
        }

        /// <summary>
        /// subscribe to a topic
        /// </summary>
        /// <param name="topic">the topic you want to subscribe to</param>
        /// <param name="callback">the callback you want to have invoked when an event with that topic is published</param>
        /// <returns>the subscriber id (use this to unsubscribe!)</returns>
        internal static string Subscribe(string topic, Action<IEvent> callback)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                Init();
#endif
            CheckOrAddTopic(topic);
            return GenerateId(topic, _events[topic].AddSubscriber<IEvent>(callback));
        }

        /// <summary>
        /// subscribe to event, regardless of topic
        /// </summary>
        /// <param name="callback">the callback you want to have invoked when an event with that topic is published</param>
        /// <returns>the subscriber id (use this to unsubscribe!)</returns>
        internal static string Subscribe(Action<IEvent> callback)
        {
            return Subscribe(TopicLess, callback);
        }

        /// <summary>
        /// Subscribe to a topic, with some extra options
        /// </summary>
        /// <param name="topic">the topic you want to subscribe to</param>
        /// <param name="callback">the callback you want to have invoked when event is published</param>
        /// <param name="filter">a filter you want to apply on events of this topic when it is published</param>
        /// <param name="queryHandler">a query callback for already existing persistent events that comply with the filter</param>
        /// <typeparam name="T">type filter</typeparam>
        /// <returns>subscriber id</returns>
        internal static string Subscribe<T>(string topic, Action<T> callback, Func<T, bool> filter = null,
            Action<List<T>> queryHandler = null) where T : IEvent
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                Init();
#endif
            CheckOrAddTopic(topic);
            return GenerateId(topic, _events[topic].AddSubscriber(callback, filter, queryHandler));
        }
        
        /// <summary>
        /// Subscribe to an event regardless of topic, with some extra options
        /// </summary>
        /// <param name="callback">the callback you want to have invoked when event is published</param>
        /// <param name="filter">a filter you want to apply on events of this topic when it is published</param>
        /// <param name="queryHandler">a query callback for already existing persistent events that comply with the filter</param>
        /// <typeparam name="T">type filter</typeparam>
        /// <returns>subscriber id</returns>
        internal static string Subscribe<T>(Action<T> callback, Func<T, bool> filter = null,
            Action<List<T>> queryHandler = null) where T : IEvent
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                Init();
#endif
            CheckOrAddTopic(TopicLess);
            return GenerateId(TopicLess, _events[TopicLess].AddSubscriber(callback, filter, queryHandler));
        }

        /// <summary>
        /// subscribe to a topic when the lifetime of your object supports scene (un)loads
        /// These subscribers will not get purged when a scene is reloaded
        /// </summary>
        /// <param name="topic">the topic you want to subscribe to</param>
        /// <param name="callback">the callback you want to have invoked when an event with that topic is published</param>
        /// <returns>the subscriber id (use this to unsubscribe!)</returns>
        internal static string SubscribeStatic(string topic, Action<IEvent> callback)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                Init();
#endif
            CheckOrAddTopic(topic);
            return GenerateId(topic, _events[topic].AddSubscriber<IEvent>(callback, null, null, true));
        }
        
        /// <summary>
        /// subscribe to an event regardless of topic when the lifetime of your object supports scene (un)loads
        /// These subscribers will not get purged when a scene is reloaded
        /// </summary>
        /// <param name="topic">the topic you want to subscribe to</param>
        /// <param name="callback">the callback you want to have invoked when an event with that topic is published</param>
        /// <returns>the subscriber id (use this to unsubscribe!)</returns>
        internal static string SubscribeStatic(Action<IEvent> callback)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                Init();
#endif
            CheckOrAddTopic(TopicLess);
            return GenerateId(TopicLess, _events[TopicLess].AddSubscriber<IEvent>(callback, null, null, true));
        }

        /// <summary>
        /// Subscribe to a topic, with some extra options,when the lifetime of your object supports scene (un)loads
        /// These subscribers will not get purged when a scene is reloaded
        /// </summary>
        /// <param name="topic">the topic you want to subscribe to</param>
        /// <param name="callback">the callback you want to have invoked when event is published</param>
        /// <param name="filter">a filter you want to apply on events of this topic when it is published</param>
        /// <param name="queryHandler">a query callback for already existing persistent events that comply with the filter</param>
        /// <typeparam name="T">type filter</typeparam>
        /// <returns>subscriber id</returns>
        internal static string SubscribeStatic<T>(string topic, Action<T> callback, Func<T, bool> filter = null,
            Action<List<T>> queryHandler = null) where T : IEvent
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                Init();
#endif
            CheckOrAddTopic(topic);
            return GenerateId(topic, _events[topic].AddSubscriber(callback, filter, queryHandler, true));
        }
        
        /// <summary>
        /// Subscribe to an event regardless of topic, with some extra options,when the lifetime of your object supports scene (un)loads
        /// These subscribers will not get purged when a scene is reloaded
        /// </summary>
        /// <param name="callback">the callback you want to have invoked when event is published</param>
        /// <param name="filter">a filter you want to apply on events of this topic when it is published</param>
        /// <param name="queryHandler">a query callback for already existing persistent events that comply with the filter</param>
        /// <typeparam name="T">type filter</typeparam>
        /// <returns>subscriber id</returns>
        internal static string SubscribeStatic<T>(Action<T> callback, Func<T, bool> filter = null,
            Action<List<T>> queryHandler = null) where T : IEvent
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                Init();
#endif
            CheckOrAddTopic(TopicLess);
            return GenerateId(TopicLess, _events[TopicLess].AddSubscriber(callback, filter, queryHandler, true));
        }

        /// <summary>
        /// Seek through the persisted events for a certain topic
        /// </summary>
        /// <param name="topic">the topic you want to seek</param>
        /// <param name="filter">the filer the events must comply to</param>
        /// <typeparam name="T">type param</typeparam>
        /// <returns>the list of already existing events on the bus that match the topic</returns>
        internal static List<T> Seek<T>(string topic, Func<T, bool> filter) where T : IEvent
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                Init();
#endif
            CheckOrAddTopic(topic);
            return _events[topic].Seek(filter);
        }
        
        /// <summary>
        /// Seek through the persisted events regardless of topic
        /// </summary>
        /// <param name="filter">the filer the events must comply to</param>
        /// <typeparam name="T">type param</typeparam>
        /// <returns>the list of already existing events on the bus that match the topic</returns>
        internal static List<T> Seek<T>(Func<T, bool> filter) where T : IEvent
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                Init();
#endif
            CheckOrAddTopic(TopicLess);
            return _events.SelectMany(e => e.Value.Seek(filter)).ToList();
        }

        /// <summary>
        /// unsubscribe 
        /// </summary>
        /// <param name="id">the Id</param>
        /// <returns>true if succeeds to unsubscribe</returns>
        internal static bool Unsubscribe(string id)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                Init();
#endif
            var topic_guid = id.Split(IdSeparator);
            return _events.ContainsKey(topic_guid[0]) && _events[topic_guid[0]].RemoveSubscriber(topic_guid[1]);
        }

        /// <summary>
        /// generate a custom id that consists of the topic and the gui id
        /// </summary>
        /// <param name="topic">the topic string</param>
        /// <param name="guid">the guid</param>
        /// <returns>topic|guid</returns>
        private static string GenerateId(string topic, string guid)
        {
            return $"{topic}{IdSeparator}{guid}";
        }

        /// <summary>
        /// check if the topic exists, else add it
        /// </summary>
        /// <param name="topic">the topic to check, or add</param>
        private static void CheckOrAddTopic(string topic)
        {
            if (!_events.ContainsKey(topic))
                _events.Add(topic, new EventStream());
        }
    }
}