using System;
using System.Collections.Generic;
using System.Linq;

namespace HamerSoft.UniventBus
{
    public class EventStream
    {
        private const string DEFAULT_SUBSCRIBER_KEY = "default";
        private const string STATIC_SUBSCRIBER_KEY = "static";
        private List<IPersistableEvent> _events;
        private Dictionary<string, Dictionary<string, Action<IEvent>>> _subscribers;

        internal EventStream()
        {
            _events = new List<IPersistableEvent>();
            CreateSubscribers();
        }

        private void CreateSubscribers()
        {
            _subscribers = new Dictionary<string, Dictionary<string, Action<IEvent>>>
            {
                {DEFAULT_SUBSCRIBER_KEY, new Dictionary<string, Action<IEvent>>()},
                {STATIC_SUBSCRIBER_KEY, new Dictionary<string, Action<IEvent>>()}
            };
        }

        internal void Clear()
        {
            _events?.ForEach(e => e.Released -= PersistableEventReleased_Handler);
            _events = new List<IPersistableEvent>();
            CreateSubscribers();
        }

        internal void ClearDefaultSubscribers()
        {
            _subscribers[DEFAULT_SUBSCRIBER_KEY] = new Dictionary<string, Action<IEvent>>();
        }

        internal void Publish(IEvent evt)
        {
            if (evt is IPersistableEvent p)
            {
                p.Released += PersistableEventReleased_Handler;
                _events.Insert(0, p);
            }

            _subscribers.Values.ToList().SelectMany(k=>k.Values).ToList().ForEach(s => s?.Invoke(evt));
        }
        
        internal void PublishTopicLess(IEvent evt)
        {
            _subscribers.Values.ToList().SelectMany(k=>k.Values).ToList().ForEach(s => s?.Invoke(evt));
        }

        internal string AddSubscriber<T>(Action<T> subscriber, Func<T, bool> filter = null,
            Action<List<T>> queryHandler = null, bool isStatic = false) where T : IEvent
        {
            return AddSubscriber(Guid.NewGuid().ToString(), subscriber, filter, queryHandler, isStatic);
        }
        
        internal string AddSubscriber<T>(string id, Action<T> subscriber, Func<T, bool> filter = null,
            Action<List<T>> queryHandler = null, bool isStatic = false) where T : IEvent
        {
            if (queryHandler != null && filter != null)
                queryHandler.Invoke(Seek(filter));

            void WrapCallback(IEvent evt)
            {
                if (!(evt is T e))
                    return;
                if (filter == null || filter.Invoke(e))
                    subscriber?.Invoke(e);
            }

            if (isStatic)
                _subscribers[STATIC_SUBSCRIBER_KEY].Add(id, WrapCallback);
            else
                _subscribers[DEFAULT_SUBSCRIBER_KEY].Add(id, WrapCallback);
            return id;
        }

        internal bool RemoveSubscriber(string id)
        {
            return RemoveSubscriber(DEFAULT_SUBSCRIBER_KEY,id) || RemoveSubscriber(STATIC_SUBSCRIBER_KEY,id);
        }

        private bool RemoveSubscriber(string targetDictionary, string id)
        {
            bool result = _subscribers[targetDictionary].ContainsKey(id);
            if (result)
                _subscribers[targetDictionary].Remove(id);
            return result;
        }

        internal List<T> Seek<T>(Func<T, bool> filter) where T : IEvent
        {
            return _events.Where(e => e is T et && filter.Invoke(et)).Cast<T>().ToList();
        }

        private void PersistableEventReleased_Handler(IPersistableEvent persistedEvent)
        {
            persistedEvent.Released -= PersistableEventReleased_Handler;
            _events.Remove(persistedEvent);
        }
    }
}